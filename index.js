require('dotenv').config()
const HetznerCloud = require('hcloud-js')
const hetznerClient = new HetznerCloud.Client(process.env.HETZNER_TOKEN)
const Slimbot = require('slimbot')
const slimbot = new Slimbot(process.env.TELEGRAM_TOKEN)
const bent = require('bent')
const getJSON = bent('json')
const sshClient = require('ssh2').Client
const { rejects } = require('assert')

// TELEGRAM LISTENER
slimbot.on('message', (m) => {
    if (m.chat.id != process.env.TELEGRAM_CHAT_ID) {
        return
    }

    try {
        const reCmd = /^\/([a-zA-Z0-9\-\_]+)(?:\@([a-zA-Z0-9\-\_]+))?$/
        const matchCmd = m.text.match(reCmd)

        switch (matchCmd[1]) {
            case 'status':
                cmdStatus()
                break
        
            case 'restart':
                cmdRestart()
                break
        
            case 'reboot':
                cmdReboot()
                break
        
            case 'upgrade':
                cmdChangeType(true)
                break
        
            case 'downgrade':
                cmdChangeType(false)
                break
        
            default:
                break
        }
    } catch (error) {
        process.exit(1)
    }
})

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms))
}

const sendMsg = async (text, message) => {
    if (message) {
        if (text !== message.text) {
            const res = await slimbot.editMessageText(process.env.TELEGRAM_CHAT_ID, message.message_id, text)
            return res.result
        } else {
            return message
        }
    } else {
        const res = await slimbot.sendMessage(process.env.TELEGRAM_CHAT_ID, text)
        return res.result
    }
}

const getServer = async () => {
    return await hetznerClient.servers.get(process.env.VPS_ID)
}
const getAction = async (actionId) => {
    return await hetznerClient.actions.get(actionId)
}

const executeSSHCommands = async (command) => {
    return new Promise(resolve => {
        const conn = new sshClient()
        conn.on('ready', function() {
            console.log('Client :: ready')
            conn.exec(command, function(err, stream) {
                if (err) throw err
                stream.on('close', function(code, signal) {
                    console.log('Stream :: close :: code: ' + code + ', signal: ' + signal)
                    resolve()
                conn.end()
                }).on('data', function(data) {
                    console.log('STDOUT: ' + data)
                }).stderr.on('data', function(data) {
                    console.log('STDERR: ' + data)
                })
            })
        }).connect({
            host: process.env.SSH_HOST,
            port: process.env.SSH_PORT,
            username: process.env.SSH_USER,
            privateKey: require('fs').readFileSync('./key')
        })
    })
}

const cmdStatus = async () => {
    const server = await getServer()
    const mode = server.serverType.name == process.env.VPS_TYPE_UPGRADE
                    ? 'upgraded'
                    : 'downgraded'
    
    const response = `The VPS is ${server.status} (${mode})`
    sendMsg(response)

    if (server.status !== 'running') {
        return
    }

    const owncastServers = process.env.OWNCAST_SERVERS.split(',')
    owncastServers.forEach(async (domain) => {
        getJSON(`https://${domain}/api/status`)
        .then(res => {
            sendMsg(`${domain} is ${res.online ? 'live' : 'NOT live'}`)
        })
        .catch(err => {
            sendMsg(`${domain} could not be reached`)
        })
    })
}

const cmdRestart = async () => {
    const server = await getServer()

    if (server.status !== 'running') {
        sendMsg(`The VPS is currently ${server.status}, cannot reset`)
        return
    }

    // RESET
    let msg = await sendMsg(`The VPS is resetting (0%)`)
    try {
        await executeSSHCommands(process.env.VPS_COMMANDS_DOWN)
        await executeSSHCommands(process.env.VPS_COMMANDS_UP)
        msg = await sendMsg(`The VPS is resetting (done)`, msg)
    } catch(err) {
        msg = await sendMsg(`The VPS is resetting (error)`, msg)
    }
}

const cmdReboot = async () => {
    const server = await getServer()

    if (server.status !== 'running' && server.status !== 'off') {
        sendMsg(`The VPS is currently ${server.status}, cannot reboot`)
        return
    }

    // EXECUTE DOWN COMMAND
    let msg = await sendMsg(`The VPS is executing DOWN command (0%)`)
    try {
        await executeSSHCommands(process.env.VPS_COMMANDS_DOWN)
        msg = await sendMsg(`The VPS is executing DOWN command (done)`, msg)
    } catch(err) {
        msg = await sendMsg(`The VPS is executing DOWN command (error)`, msg)
    }

    // REBOOT
    msg = await sendMsg(`The VPS is rebooting (0%)`)
    if (server.status === 'running') {
        await server.reboot()
        .then(async (res) => {
            return await new Promise(resolve => {
                const interval = setInterval(async () => {
                    const actionUpdate = await getAction(res.id)
                    msg = await sendMsg(`The VPS is rebooting (${actionUpdate.progress}%)`, msg)
                    if (actionUpdate.progress >= 100) {
                        resolve()
                        clearInterval(interval)
                    }
                }, 1000)
            })
        })
        .then((res) => {
            sendMsg(`The VPS is rebooting (done)`, msg)
        })
        .catch((err) => {
            sendMsg(`Error: ${err.message}`)
        })
    } else {
        await server.powerOn()
        .then(async (res) => {
            return await new Promise(resolve => {
                const interval = setInterval(async () => {
                    const actionUpdate = await getAction(res.id)
                    msg = await sendMsg(`The VPS is powering on (${actionUpdate.progress}%)`, msg)
                    if (actionUpdate.progress >= 100) {
                        resolve()
                        clearInterval(interval)
                    }
                }, 1000)
            })
        })
        .then((res) => {
            sendMsg(`The VPS is powering on (done)`, msg)
        })
        .catch((err) => {
            sendMsg(`Error: ${err.message}`)
        })
    }

    // WAIT FOR SERVER STATUS TO BE RUNNING
    await new Promise(resolve => {
        const interval = setInterval(async () => {
            const serverUpdate = await getServer()
            if (serverUpdate.status == 'running') {
                resolve()
                clearInterval(interval)
            }
        }, 1000)
    })

    // SLEEP WHILE OS IS STARTING
    msg = await sendMsg(`The VPS is booting (20 seconds)`)
    await sleep(20000)
    msg = await sendMsg(`The VPS is booting (done)`, msg)

    // EXECUTE UP COMMAND
    msg = await sendMsg(`The VPS is executing UP command (0%)`)
    try {
        await executeSSHCommands(process.env.VPS_COMMANDS_UP)
        msg = await sendMsg(`The VPS is executing UP command (done)`, msg)
    } catch(err) {
        msg = await sendMsg(`The VPS is executing UP command (error)`, msg)
    }
}

const cmdChangeType = async (isUpgrade) => {
    const server = await getServer()

    if (server.status !== 'running' && server.status !== 'off') {
        sendMsg(`The VPS is currently ${server.status}, cannot ${isUpgrade ? 'upgrading' : 'downgrading'} right now`)
        return
    }

    // EXECUTE DOWN COMMAND
    let msg = await sendMsg(`The VPS is executing DOWN command (0%)`)
    try {
        await executeSSHCommands(process.env.VPS_COMMANDS_DOWN)
        msg = await sendMsg(`The VPS is executing DOWN command (done)`, msg)
    } catch(err) {
        msg = await sendMsg(`The VPS is executing DOWN command (error)`, msg)
    }

    // SHUT DOWN
    msg = await sendMsg(`The VPS is shutting down (0%)`)
    await server.shutdown()
    .then(async (res) => {
        return await new Promise(resolve => {
            const interval = setInterval(async () => {
                const actionUpdate = await getAction(res.id)
                msg = await sendMsg(`The VPS is shutting down (${actionUpdate.progress}%)`, msg)
                if (actionUpdate.progress >= 100) {
                    resolve()
                    clearInterval(interval)
                }
            }, 1000)
        })
    })
    .then((res) => {
        sendMsg(`The VPS is shutting down (done)`, msg)
    })
    .catch((err) => {
        sendMsg(`Error: ${err.message}`)
    })

    // WAIT FOR SERVER STATUS TO BE OFF
    await new Promise(resolve => {
        const interval = setInterval(async () => {
            const serverUpdate = await getServer()
            if (serverUpdate.status == 'off') {
                resolve()
                clearInterval(interval)
            }
        }, 1000)
    })

    // UPGRADE or DOWNGRADE
    msg = await sendMsg(`The VPS is ${isUpgrade ? 'upgrading' : 'downgrading'} (0%)`)
    await server.changeType(isUpgrade ? process.env.VPS_TYPE_UPGRADE : process.env.VPS_TYPE_DOWNGRADE, false)
    .then(async (res) => {
        return await new Promise(resolve => {
            const interval = setInterval(async () => {
                const actionUpdate = await getAction(res.id)
                msg = await sendMsg(`The VPS is ${isUpgrade ? 'upgrading' : 'downgrading'} (${actionUpdate.progress}%)`, msg)
                if (actionUpdate.progress >= 100) {
                    resolve()
                    clearInterval(interval)
                }
            }, 1000)
        })
    })
    .then((res) => {
        sendMsg(`The VPS is ${isUpgrade ? 'upgrading' : 'downgrading'} (done)`, msg)
    })
    .catch((err) => {
        sendMsg(`Error: ${err.message}`)
    })

    // WAIT FOR SERVER STATUS TO BE RUNNING
    await new Promise(resolve => {
        const interval = setInterval(async () => {
            const serverUpdate = await getServer()
            if (serverUpdate.status == 'running') {
                resolve()
                clearInterval(interval)
            }
        }, 1000)
    })

    // SLEEP WHILE OS IS STARTING
    msg = await sendMsg(`The VPS is booting (20 seconds)`)
    await sleep(20000)
    msg = await sendMsg(`The VPS is booting (done)`, msg)

    // EXECUTE UP COMMAND
    msg = await sendMsg(`The VPS is executing UP command (0%)`)
    try {
        await executeSSHCommands(process.env.VPS_COMMANDS_UP)
        msg = await sendMsg(`The VPS is executing UP command (done)`, msg)
    } catch(err) {
        msg = await sendMsg(`The VPS is executing UP command (error)`, msg)
    }
}

// START TELEGRAM BOT
slimbot.startPolling()